const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const app = express();
const port = 8080;

const TOKEN_SECRET =
  "88e9aa1b6430e400064fb0e3b3a876803a5cebd556b10d1d3d010b7b8b2c121f8641f72493e0b05f900eb9878a347c719d06475f46a99a291d7f1bb26de71934";

const authenticated = (req, res, next) => {
  const auth_header = req.headers["authorization"];
  const token = auth_header && auth_header.split(" ")[1];
  console.log(token);
  if (!token) return res.sendStatus(401);

  jwt.verify(token, TOKEN_SECRET, (err, info) => {
    if (err) return res.sendStatus(403);
    req.username = info.username;
    next();
  });
};

app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/api/info", authenticated, (req, res) => {
  res.send({ ok: 1 ,username:req.username});
});

app.post("/api/login", bodyParser.json(), async (req, res) => {
  let token = req.body.token;
  let result = await axios.get("https://graph.facebook.com/me", {
    params: {
      fields: "id,name,email",
      access_token: token,
    },
  });
  console.log(result.data);
  if (!result.data.id) {
    res.sendStatus(403);
    return;
  }

  let data = {
    username: result.data.email,
  };
  let access_token = jwt.sign(data, TOKEN_SECRET, { expiresIn: "1800s" });
  res.send({ access_token, username: data.username });
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
